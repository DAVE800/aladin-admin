import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-conseiller',
  templateUrl: './conseiller.component.html',
  styleUrls: ['./conseiller.component.scss']
})
export class ConseillerComponent implements OnInit {


  constructor(private monhttp:HttpService) { }
  orders:any=[]
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  ngOnInit(): void {
   
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
        console.log(this.orders)
        for(let i =0;i<this.orders.length;i++){
         this.orders[i].description= JSON.parse(this.orders[i].description)
        
        }
},
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        console.log( this.pendingorders)
        for(let i =0;i<this.pendingorders.length;i++){
          this.pendingorders[i].description= JSON.parse(this.pendingorders[i].description)
         }
      },
      err=>{
        console.log(err)
      }

    )
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      console.log(this.okorders)
      for(let i =0;i<this.okorders.length;i++){
        this.okorders[i].description= JSON.parse(this.okorders[i].description)
       }
    }
    ,
      err=>{
        console.log(err)
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
     for(let i =0;i<this.okorders.length;i++){
      this.cancelorders[i].description= JSON.parse(this.cancelorders[i].description)
     }
    },
    err=>{
      console.log(err)
    }
  )
  }

updateStatus(event:any){
 let id = event.target.id
 if(id!=undefined){
  
   let data={id:id,status:'pending'}
   this.monhttp.Updatestatus(data).subscribe(
     res=>{
       console.log(res)
       if(res.status==true){
         this.monhttp.Getpending().subscribe(
           res=>{
             //console.log(res)
             this.pendingorders=res.data
             console.log( this.pendingorders)
           }
         )
       }
     },
     err=>{
       console.log(err)
     }
   )
 }
 
}
updateok(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'ok'}
    this.monhttp.updateok2(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.GetordersLivre().subscribe(
            res=>{
              //console.log(res)
              this.okorders=res.data
              console.log(this.okorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}
updatecancel(event:any){
  let id= event.target.id
  if(id!=undefined){
    let data={id:id, status:'rejected'}
    this.monhttp.Updatecancel(data).subscribe(
      res=>{
        if(res.status==true){
          this.monhttp.getCancel().subscribe(
            res=>{
              //console.log(res)
              this.cancelorders=res.data
              console.log(this.cancelorders)
            }
          )
        }
      },
      err=>{
        console.log(err)
      }
    )
  }
}



logout(){
  localStorage.removeItem('user');
  localStorage.removeItem('role');
  location.reload();
}
}
