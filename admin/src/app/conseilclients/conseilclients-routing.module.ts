import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConseillerComponent } from './conseiller/conseiller.component';
import { CCGuard } from '../services/cc.guard';

const routes: Routes = [
  {path:'service',component:ConseillerComponent,canActivate:[CCGuard]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConseilclientsRoutingModule { }
