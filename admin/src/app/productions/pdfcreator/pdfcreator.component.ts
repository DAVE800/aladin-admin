import { Component, OnInit,Input } from '@angular/core';
import html2canvas from 'html2canvas';
import { PDFDocument } from 'pdf-lib';
import { HttpService } from 'src/app/services/http.service';
import jsPDFInvoiceTemplate,{ OutputType, jsPDF }  from "jspdf-invoice-template";
declare var require: any

var $ = require("jquery");
@Component({
  selector: 'app-pdfcreator',
  templateUrl: './pdfcreator.component.html',
  styleUrls: ['./pdfcreator.component.scss']
})
export class PdfcreatorComponent implements OnInit {
@Input() face1:any;
@Input() face2:any;
@Input() pdf1:any;
@Input() pdf2:any;
  constructor(private service :HttpService) { }

  ngOnInit(): void {
   // jsPDFInvoiceTemplate.default();
   
   
  }


  async  embedImages() {
    const jpgUrl = this.face1;
    const pngUrl = this.face2;
    const f1 = this.pdf1;
    const f2 = this.pdf2;
    const jpgImageBytes = await fetch(jpgUrl).then((res) => res.arrayBuffer())
    const pngImageBytes = await fetch(pngUrl).then((res) => res.arrayBuffer())

    const jpgImageBytes1 = await fetch(f1).then((res) => res.arrayBuffer())
    const pngImageBytes2 = await fetch(f2).then((res) => res.arrayBuffer())
     
     
    const pdfDoc = await PDFDocument.create();
  
    const jpgImage = await pdfDoc.embedPng(jpgImageBytes);
    const pngImage = await pdfDoc.embedPng(pngImageBytes);
    const p1 = await pdfDoc.embedPng(jpgImageBytes1);
    const p2 = await pdfDoc.embedPng(pngImageBytes2);

  
    const jpgDims = jpgImage.scale(0.5)
    const pngDims = pngImage.scale(0.5)
    const pngDims1=p1.scale(0.5);
    const jpgDims1=p2.scale(0.5);

    const page1 = pdfDoc.addPage()
  
    page1.drawImage(jpgImage, {
      x: page1.getWidth() / 2 - jpgDims.width / 2,
      y: page1.getHeight() / 2 - jpgDims.height / 2 ,
      width: jpgDims.width,
      height: jpgDims.height,
    })
    const page3 = pdfDoc.addPage()

    page3.drawImage(p1, {
      x: page3.getWidth() / 2 - pngDims1.width / 2 ,
      y: page3.getHeight() / 2 - pngDims1.height /2,
      width: pngDims1.width,
      height: pngDims1.height,
    });

    const page2 =pdfDoc.addPage();
    page2.drawImage(pngImage, {
      x: page2.getWidth() / 2 - pngDims.width / 2 ,
      y: page2.getHeight() / 2 - pngDims.height/ 2,
      width: pngDims.width,
      height: pngDims.height,
    })
   
    const page4 = pdfDoc.addPage();
    page4.drawImage(p2, {
      x: page4.getWidth() / 2 - jpgDims1.width / 2,
      y: page4.getHeight() / 2 - jpgDims1.height / 2 ,
      width: jpgDims1.width,
      height: jpgDims1.height,
    })
    const page5 = pdfDoc.addPage() 
      page5.drawText('david k', { 
        x:page5.getWidth()/3,
        y:page5.getHeight()/2

        })
  
    const pdfBytes = await pdfDoc.saveAsBase64({ dataUri: true });
    let pdf = document.getElementById("pdf");
    pdf?.setAttribute('href',pdfBytes);
    this.service.triggerMouse(pdf);

  }
  
  renderPdf(){
   this.embedImages()  
  // this.MakePdf()

  }


  MakePdf(){
  var doc = new jsPDF({
    format: [4, 2],
    orientation:"portrait",

  });
  doc.setFontSize(3)
  
  let html:any=document?.getElementById('mypdf')
  html2canvas(html,
    {useCORS:true}).then(function(canvas) {
    document.body.appendChild(canvas);
      var img = canvas.toDataURL();
      doc.addImage(img, 'PNG', 0, 0, 297, 210);
      doc.save('sample-file.pdf');
  

});

  
  }
  

}
