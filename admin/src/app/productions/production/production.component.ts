import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-production',
  templateUrl: './production.component.html',
  styleUrls: ['./production.component.scss']
})
export class ProductionComponent implements OnInit {

  constructor(private monhttp:HttpService ) {}
  orders:any=[]
  pendingorders:any=[]
  okorders:any=[]
  cancelorders:any=[]
  tab:any=[];
  cpt=0
  modal='#'
  cacher=false;
  prod_face1:any;
  prod_face2:any;
  pdf1:any;
  pdf2:any;
  ngOnInit(): void {
   
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        this.orders=res.data
        for(let i =0;i<this.orders.length;i++){
         this.orders[i].description= JSON.parse(this.orders[i].description);
         console.log(this.orders.description)

        
        }
  },
      err=>{
        console.log(err)
      }

    )
    this.monhttp.Getpending().subscribe(
      res=>{
        this.pendingorders=res.data
        console.log( this.pendingorders)
        for(let i =0;i<this.pendingorders.length;i++){
          this.pendingorders[i].description= JSON.parse(this.pendingorders[i].description)
         }
      },
      err=>{
        console.log(err);
      }

    )
    
  this.monhttp.GetordersLivre().subscribe(
    res=>{
      this.okorders=res.data
      console.log(this.okorders)
      for(let i =0;i<this.okorders.length;i++){
        this.okorders[i].description= JSON.parse(this.okorders[i].description)
       }
    }
    ,
      err=>{
        console.log(err);
      }
  )
  this.monhttp.getCancel().subscribe(
    res=>{
     this.cancelorders=res.data
     console.log(this.cancelorders)
     for(let i =0;i<this.cancelorders.length;i++){
      this.cancelorders[i].description= JSON.parse(this.cancelorders[i].description)
     }
    },
    err=>{
      console.log(err)
    }
  )
  }
  
  toggel(){
    this.cacher=true
  }
    updateStatus(event:any){
    let id = event.target.id
    if(id!=undefined){
      
      let data={id:id,status:'pending'}
      this.monhttp.Updatestatus(data).subscribe(
        res=>{
          console.log(res)
          if(res.status==true){
            this.monhttp.Getpending().subscribe(
              res=>{
                //console.log(res)
                this.pendingorders=res.data
                console.log( this.pendingorders)
              }
            )
          }
        },
        err=>{
          console.log(err)
        }
      )
    }
    
    }
    updateok(event:any){
      let id= event.target.id
      if(id!=undefined){
        let data={id:id, status:'ok'}
        this.monhttp.updateok2(data).subscribe(
          res=>{
            if(res.status==true){
              this.monhttp.GetordersLivre().subscribe(
                res=>{
                  //console.log(res)
                  this.okorders=res.data
                  console.log(this.okorders)
                }
              )
            }
          },
          err=>{
            console.log(err)
          }
        )
      }
    }
    updatecancel(event:any){
      let id= event.target.id
      if(id!=undefined){
        let data={id:id, status:'rejected'}
        this.monhttp.Updatecancel(data).subscribe(
          res=>{
            if(res.status==true){
              this.monhttp.getCancel().subscribe(
                res=>{
                  //console.log(res)
                  this.cancelorders=res.data
                  console.log(this.cancelorders)
                }
              )
            }
          },
          err=>{
            console.log(err)
          }
        )
      }
    }

download(){
  let download=document.getElementById("imdone");
  this.monhttp.triggerMouse(download);
}


Zoomimg(event:any){
    
      if(this.cacher){
        let id=event.target.id;
        let src1=this.orders[+id].description.face1;
        let src2=this.orders[+id].description.face2;
        this.pdf1=src1;
        this.pdf2=src2;
        this.prod_face1=this.orders[+id].description.production_f1;
        this.prod_face2=this.orders[+id].description.production_f2;

        document.getElementById('img1')?.setAttribute('src',src1);
        document.getElementById('img2')?.setAttribute('src',src2);
        if(this.cpt>0){
          let modal=document.getElementById('modalbtn');
          this.monhttp.triggerMouse(modal);
        }else{
          this.cpt=this.cpt+1;
        }
        console.log(id);
      }else{
        this.toggel();
        let id=event.target.id;
        let src1=this.orders[+id].description.face1;
        let src2=this.orders[+id].description.face2;
        document.getElementById('img1')?.setAttribute('src',src1);
        document.getElementById('img2')?.setAttribute('src',src2);
        if(this.cpt>0){
          let modal=document.getElementById('modalbtn');
          this.monhttp.triggerMouse(modal);
        }else{
          this.cpt=this.cpt+1;
        }
  
      }
     

    }



    getOrderInfo(event:any){
      

    }
}
