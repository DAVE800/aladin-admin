import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductionComponent } from './production/production.component';
import { ProductionsRoutingModule } from './productions-routing.module';
import { PdfcreatorComponent } from './pdfcreator/pdfcreator.component';


@NgModule({
  declarations: [
    ProductionComponent,
    PdfcreatorComponent 
    ],
  imports: [
    CommonModule,
    ProductionsRoutingModule
  ]
})
export class ProductionsModule { }
